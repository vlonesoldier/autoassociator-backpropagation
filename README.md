# Autoassociator

Program based on machine learning - backward propagation of errors algorithm

## Description

It is a window application that shows up an image drawn by self-teaching algorithm. The implementation extends backpropagation whose backbone stands for neural networks consisted of layers that can propagate newly calculated weights.
