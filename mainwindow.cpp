#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qpainter.h>
#include <math.h>
#include <cstdlib>
#include <random>
#include <time.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    srand ( time(NULL) );

    img = new QImage(":/images/yelawolf.jpg");
    img2 = new QImage(height, width, QImage::Format_RGB32);
    this->setMouseTracking(true);
    initWeights();
    initBlankImg();
    drawImg();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initWeights() {

    for(int i = 0; i < layers; i++){

        for(int j = 0; j < 2; j++)
            initial_weights[i][j] = ((double)rand() / double(RAND_MAX)) - 0.5;

        for(int j = 0; j < 3; j++)
            hidden_weights[i][j] = (rand() / double(RAND_MAX)) - 0.5;

        hidden_bias[i] = ((double)rand() / double(RAND_MAX));
    }

    for(int i = 0; i < 3; i++){
        output_bias[i] = ((double)rand() / double(RAND_MAX));
    }
}

void MainWindow::initBlankImg() {

    for(int i = 0; i < height; i++)
        for(int j = 0; j < width; j++)
            for(int k = 0; k < 3; k++)
                max_predicted_output[k][i][j] = 0;

}

void MainWindow::drawImg() {

    int _x, _y;

    for(int i = 0; i < 5 * height; i++){

        for(int j = 0; j < 5 * width; j++){

            _x = rand() % height;
            _y = rand() % width;

            forward(_x / double(height), _y / double(width));
            backpropagation(_x / double(height), _y / double(width));

            for(int k = 0; k < 3; k++) {

                if(abs(getColorAndSet(_x, _y, k) - max_predicted_output[k][_x][_y]) >
                    abs(getColorAndSet(_x, _y, k) - predicted_output[k][_x][_y])) {

                    max_predicted_output[k][_x][_y] = predicted_output[k][_x][_y];

                    setPixel(
                        (unsigned char*)img2->bits(),
                        height,
                        width,
                        _x,
                        _y,
                        max_predicted_output[0][_x][_y] * 255,
                        max_predicted_output[1][_x][_y] * 255,
                        max_predicted_output[2][_x][_y] * 255
                        );
                }
            }
        }
    }
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.drawImage(0, 0, *img);
    p.drawImage(300, 0, *img2);
}

double MainWindow::getColorAndSet(int x, int y, int choose){

    QRgb pixColor = img->pixel(x, y);
    double red   = qRed(pixColor);
    double green = qGreen(pixColor);
    double blue  = qBlue(pixColor);

    red /= 255.0;
    green /= 255.0;
    blue /= 255.0;

    switch(choose){

    case 0:
        return red;
        break;
    case 1:
        return green;
        break;
    case 2:
        return blue;
        break;
    }

    return 0;
}


void MainWindow::setPixel(unsigned char* bits, int height, int width, int x, int y, int r, int g, int b)
{
    if(x > 0 && y > 0 && x < width && y < height)
    {
        bits[4 * (x + y * width)] = std::min(std::max(0,r),255);
        bits[4 * (x + y * width) + 1] = std::min(std::max(0,g),255);
        bits[4 * (x + y * width) + 2] = std::min(std::max(0,b),255);
    }

}

double MainWindow::sigmoid(double x){
    return double(1.0/(1 + pow(M_E, (-x))));
}

double MainWindow::sigmoid_derivative(double x)
{
    return double(x*(1-x));
}

void MainWindow::forward(double x, double y){
    int _x = x * height;
    int _y = y * width;

    for(int i = 0; i < layers; i++){
        hidden_layer_activation[i] = 0;
    }

    for(int i = 0; i < 3; i++){
        output_layer_activation[i] = 0;
    }

    for(int i = 0; i < layers; i++){
        hidden_layer_activation[i] += (initial_weights[i][0] * x) + (initial_weights[i][1] * y);

        hidden_layer_activation[i] += hidden_bias[i];
        hidden_layer_output[i] = sigmoid(hidden_layer_activation[i]);
    }

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < layers; j++)
            output_layer_activation[i] += hidden_layer_output[j] * hidden_weights[j][i];

        output_layer_activation[i] += output_bias[i];
    }

    for(int i = 0; i < 3; i++)
        predicted_output[i][_x][_y] = sigmoid(output_layer_activation[i]); 
}

void MainWindow::backpropagation(double x, double y){
    double r = 0.1;

    int _x = x * height;
    int _y = y * width;

    error[0] = getColorAndSet(_x, _y, 0) - predicted_output[0][_x][_y];
    error[1] = getColorAndSet(_x, _y, 1) - predicted_output[1][_x][_y];// - predicted_output[1];
    error[2] = getColorAndSet(_x, _y, 2) - predicted_output[2][_x][_y];// - predicted_output[2];

    d_predicted_output[0] = error[0] * sigmoid_derivative(predicted_output[0][_x][_y]);
    d_predicted_output[1] = error[1] * sigmoid_derivative(predicted_output[1][_x][_y]);
    d_predicted_output[2] = error[2] * sigmoid_derivative(predicted_output[2][_x][_y]);

    for(int i = 0; i < 3; i++)
        output_bias[i] += d_predicted_output[i] * r;


    for(int i = 0; i < layers; i++)
    {
        error_hidden_layer[i] = 0;

        for(int j = 0; j < 3; j++){

            error_hidden_layer[i] += d_predicted_output[j] * hidden_weights[i][j];

        }
        d_hidden_layer[i] = error_hidden_layer[i] * sigmoid_derivative(hidden_layer_output[i]);

        for(int j = 0; j < 3; j++){
            hidden_weights[i][j] += hidden_layer_output[i] * d_predicted_output[j] * r;
        }

        initial_weights[i][0] += x * d_hidden_layer[i] * r;
        initial_weights[i][1] += y * d_hidden_layer[i] * r;

        hidden_bias[i] += d_hidden_layer[i] * r;
    }
}
