#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qimage.h>
#include <QDir>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

struct colors{
    int red;
    int green;
    int blue;

};

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QImage *img, *img2;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent*);
    void setPixel(unsigned char*, int, int, int, int, int, int, int);
    double getColorAndSet(int, int, int);

private:
    static const int layers = 50,
        width = 200,
        height = 200;

    int bias = 1;
    double output_bias[3];
    double hidden_bias[layers];

    double initial_weights[layers][2];
    double hidden_weights[layers][3];

    double hidden_layer_activation[layers];
    double hidden_layer_output[layers];
    double d_hidden_layer[layers];
    double output_layer_activation[3];

    double predicted_output[3][height][width];
    double max_predicted_output[3][height][width];
    double d_predicted_output[3];

    double error_hidden_layer[layers];
    double error[3];

    void initBlankImg();
    void initWeights();
    void drawImg();
    void backpropagation(double, double);
    double sigmoid(double);
    double sigmoid_derivative(double);
    void forward(double, double);

    QString homeDir = QDir::currentPath();
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
